serverbench node-js

## How to build
in the terminal, type
`docker build --tag 'nodejs-fibo-benchmark' .`

## How to run:
in the terminal, type
`./docker.run`

## Where is the server?
In case you're running docker locally
`http://localhost/fibonacci`

otherwise, replace localhost with the ip of your server
`http://1.2.3.4/fibonacci`