const http = require('http');
const fs = require('fs');

const PORT = 8080;

// Aha, this fibonacci algorithm is NOT recursive!
function fib_loop(n) {
  if (n <= 1) {
    return BigInt(n);
  } else {
    let a = BigInt(0);
    let b = BigInt(1);
    let temp;
    for (let i = 2; i <= n; i++) {
      temp = a + b;
      a = b;
      b = temp;
    }
    return b.toString();
  }
}

function fib_recursive(n) {
  if (n <= 1) {
    return BigInt(n);
  } else {
    let a = BigInt(0);
    let b = BigInt(1);
    let temp;
    for (let i = 2; i <= n; i++) {
      temp = a + b;
      a = b;
      b = temp;
    }
    return b.toString();
  }
}

const server = http.createServer((req, res) => {
  if (req.url === '/fibonacci') {
    const n = 10000; // Calculate the 10,000th Fibonacci number

    const result = fib_loop(n);
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.end(`
      <html>
        <head>
          <title>10,000th Fibonacci Number</title>
        </head>
        <body>
          <h1>10,000th Fibonacci Number</h1>
          <p>The 10,000th Fibonacci number is: ${result}</p>
          <p>Result has also been saved. Check the server logs for the result.</p>
        </body>
      </html>
    `);
    
    // Store the result in a file
    fs.appendFile('fibonacci_result.txt', `Fibonacci sequence at position ${n} is: ${result}\n`, (err) => {
      if (err) throw err;
      console.log('Result saved to fibonacci_result.txt');
    });
  } else {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('Hello, World!\n');
  }
});

server.listen(PORT, () => {
  console.log(`Server running at http://localhost:${PORT}/`);
});
