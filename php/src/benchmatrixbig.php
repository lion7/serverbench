<?php
echo "<H1>Dit doet het wel:</H1>";

$argnum = $_GET['num'];

if ($argnum == "") {
    echo "usage: <URL>/bench.php?num=20";
}

echo "<BR>";

// Ensure that the bcmath extension is loaded
if (!extension_loaded('bcmath')) {
    echo "<H1>Dit doet het NIET!</H1>";

    die('BCMath extension is not available. Please enable it for arbitrary precision arithmetic.');
}

function matrixMultiply($a, $b) {
    $result = array();

    $result[0][0] = bcadd(bcmul($a[0][0], $b[0][0]) , bcmul($a[0][1] , $b[1][0]));
    $result[0][1] = bcadd(bcmul($a[0][0], $b[0][1]) , bcmul($a[0][1] , $b[1][1]));
    $result[1][0] = bcadd(bcmul($a[1][0], $b[0][0]) , bcmul($a[1][1] , $b[1][0]));
    $result[1][1] = bcadd(bcmul($a[1][0], $b[0][1]) , bcmul($a[1][1] , $b[1][1]));

    return $result;
}

function matrixPower($matrix, $n) {
    $result = array(array(1, 0), array(0, 1)); // Identity matrix

    while ($n > 0) {
        if ($n % 2 == 1) {
            $result = matrixMultiply($result, $matrix);
        }

        $matrix = matrixMultiply($matrix, $matrix);
        $n = bcdiv($n, 2);
    }

    return $result;
}

function fibonacci($n) {
    $baseMatrix = array(array(1, 1), array(1, 0));
    $resultMatrix = matrixPower($baseMatrix, $n - 1);

    return $resultMatrix[0][0];
}

if ($argnum > 0) {
    $number = $argnum;
} else {
    $number = 10;
}

$result = fibonacci($number);

echo "The ${number}th Fibonacci number using matrix exponentiation is: $result";

?>