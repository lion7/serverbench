<?php

echo "<H1>Dit doet het wel:</H1>";
$argnum = $_GET['num'];

if ($argnum == "") {
    echo "usage: <URL>/bench.php?num=20";
}

echo "<BR>";

// Ensure that the bcmath extension is loaded
if (!extension_loaded('bcmath')) {
    echo "<H1>Dit doet het NIET!</H1>";

    die('BCMath extension is not available. Please enable it for arbitrary precision arithmetic.');
}

function fibonacci($n) {
    // echo "fibon: '$n' ";
    if (-1 == bccomp("$n", "0")) {
        return "0";
    } else if (-1 == bccomp("$n", "1")) {
        // echo "<= 1<br>";
        return "1";
    } else {
        return bcadd(fibonacci(bcsub("$n", "1")), fibonacci(bcsub("$n", "2")));
    }
}

if ($argnum != "" && ((int)$argnum > 0)) {
    $number = $argnum;
} else {
    echo "$argnum should be an integer number > 0.<br>";
    $number = 0;
}
$result = fibonacci(strval($number));

// $result = bccomp("1", "1", 0);
echo "The {$number}th Fibonacci number is: $result";

?>