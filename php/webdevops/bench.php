<?php

echo "<H1>Dit doet het wel:</H1>";

// Ensure that the bcmath extension is loaded
if (!extension_loaded('bcmath')) {
    die('BCMath extension is not available. Please enable it for arbitrary precision arithmetic.');
}

function fibonacci($n) {
    if ($n <= 1) {
        return $n;
    } else {
        return bcadd(fibonacci($n - 1), fibonacci($n - 2));
    }
}

$number = 10000;
$result = fibonacci($number);

echo "The 10000th Fibonacci number is: $result";

?>