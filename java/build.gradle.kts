plugins {
    application
    kotlin("jvm") version "1.9.23"
    // jib (Java Image Builder) is used to containerize the app
    id("com.google.cloud.tools.jib") version "3.4.0"
}

group = "dev.lion7"
version = "1.0-SNAPSHOT"

application {
    mainClass = "dev.lion7.MainKt"
}

kotlin {
    jvmToolchain(21)
}

jib {
    from {
        image = "cgr.dev/chainguard/jre"
    }
    to {
        image = "axello1/java-fibo-benchmark"
    }
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("io.helidon.webserver:helidon-webserver-http2:4.0.7")
    testImplementation(kotlin("test"))
}

tasks {
    distTar {
        archiveVersion.set("")
    }

    test {
        useJUnitPlatform()
    }
}
