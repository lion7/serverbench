package dev.lion7

import java.math.BigInteger
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.time.measureTimedValue

class FibonacciTest {

    private val fn: (Int) -> BigInteger = Fibonacci::iterative

    @Test
    fun testRange() {
        val expectedTxt = javaClass.getResource("/expected.txt")!!.readText().trim()
        expectedTxt.lines().map(String::toBigInteger).forEachIndexed { i, expected ->
            val n = i + 1 // i = 0-based index
            val actual = measureTimedValue {
                fn(n)
            }
            println("Calculating $n took ${actual.duration}")
            assertEquals(expected, actual.value, "Failed for n = $n")
        }
    }
}
