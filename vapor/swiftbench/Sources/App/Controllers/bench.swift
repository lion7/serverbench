import Vapor
import BigInt

struct FibonacciController: RouteCollection {
    func boot(routes: RoutesBuilder) throws {
        routes.get("bench", ":count", use: generateFibonacci)
    }

    func generateAllFibonacci(req: Request) throws -> String {
        var fibonacciNumbers: [BigInt] = [0, 1]

        for _ in 2..<10000 {
            let nextFibonacci = fibonacciNumbers[fibonacciNumbers.count - 1] + fibonacciNumbers[fibonacciNumbers.count - 2]
            fibonacciNumbers.append(nextFibonacci)
        }

        let result = fibonacciNumbers.map { "\($0)" }.joined(separator: ", ")
        return result
    }
    
    // this is also a loop, not recursive!
    func generateFibonacci(req: Request) throws -> String {
        guard let countParam = req.parameters.get("count"), let count = Int(countParam) else {
            throw Abort(.badRequest, reason: "Invalid count parameter")
        }

        guard count > 0 else {
            throw Abort(.badRequest, reason: "Count must be greater than 0")
        }

        if count == 1 {
            return "0"
        }

        var previous = BigInt(0)
        var current = BigInt(1)

        for _ in 2 ..< count {
            let nextFibonacci = previous + current
            previous = current
            current = nextFibonacci
        }

        return "\(current)"
    }

}
